import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverviewComponent } from './overview.component';
import { ChartComponent } from '../chart/chart.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { ForecastComponent } from '../forecast/forecast.component';
import { ForecastDetailsComponent } from '../forecast-details/forecast-details.component';
import { APP_BASE_HREF } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

describe('OverviewComponent', () => {
  let component: OverviewComponent;
  let fixture: ComponentFixture<OverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        OverviewComponent,
        ChartComponent,
        ForecastComponent,
        ForecastDetailsComponent
    ],
    imports: [AppRoutingModule, HttpClientModule],
    providers: [{provide: APP_BASE_HREF, useValue: '/'}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
