import { Component, OnInit, OnDestroy } from '@angular/core';
import { FeatureService } from '../../services/feature-service.service'
import { StateService } from '../../services/state-service.service';
import { Router } from '@angular/router';
import { appConstant } from '../../app-constants';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit, OnDestroy {

  /**
   * cities array is used to store 5 europe cities name, currentWeather and forecast to display
  */
  public cities = [
    {
      id: 2759794,
      name: "Amsterdam",
      country: "NL",
      currentWeather: {},
      forecast: {}
    },
    {
      id: 2747891,
      name: "Rotterdam",
      country: "NL",
      currentWeather: {},
      forecast: {}
    },
    {
      id: 2988507,
      name: "Paris",
      country: "FR",
      currentWeather: {},
      forecast: {}
    },
    {
      id: 2761369,
      name: "Vienna",
      country: "AT",
      currentWeather: {},
      forecast: {}
    },
    {
      id: 2950159,
      name: "Berlin",
      country: "DE",
      currentWeather: {},
      forecast: {}
    }
  ];
  public weatherData: any;
  public forecastData: any;
  public todayDate = new Date();
  public homeCityObj: any;
  private sub: any;
  public chartObj: any;

  constructor(
      private router: Router,
      private featureService: FeatureService,
      private stateService: StateService
    ) { }

  /**
   * @name gotoForecast
   * @methodOf overview.component
   * @param {object} city
   * @description
   * This method navigates to forecast component
  */
  gotoForecast(city){
    this.router.navigate(['/forecast', { cityId: city.id}]);
  }

  ngOnInit() {
    this.getCityWeather();
    this.sub = this.stateService.homeCity.subscribe((cityId: string) => {
      const city = this.cities.find(obj => obj.id == parseInt(cityId));
      this.homeCityObj = city ? city : this.cities[0];
      this.getHomeCityForecast(this.homeCityObj['id'])
    })
  }

  /**
   * @name getCityWeather
   * @methodOf overview.component
   * @description
   * This method retrieves current weather data of all 5 cities
   */
  getCityWeather() {
    let cityIds = [];
    for(let city of this.cities) {
      cityIds.push(city.id);
    }
    const reqParams = {
      params: {
        id: cityIds.toString(),
        appid: appConstant.APP_ID,
        units: appConstant.UNIT
      }
    }
    this.featureService.httpOperation('getWeatherData',reqParams).subscribe(data=>{
      this.weatherData = data['list'];
      this.cities.map(city => {
        city.currentWeather = this.weatherData.find(obj => obj.id === city.id);
      });
    });
  }

  /**
   * @name getHomeCityForecast
   * @methodOf overview.component
   * @param cityId
   * @description
   * This method retrieves Forecast weather data of home city
   */
  getHomeCityForecast(cityId) {
    const reqParams = {
      params: {
        id: cityId,
        appid: appConstant.APP_ID,
        units: appConstant.UNIT
      }
    }
    this.featureService.httpOperation('getForecastWeather',reqParams).subscribe(data=>{
      this.forecastData = data;

      //For graph
      let _t = data['list'].map(res => Math.round(res.main.temp));
      let alldates = data['list'].map(res => res.dt);
      let w_Dates = [];
      alldates.forEach((res) => {
          let jsdate = new Date(res * 1000)
          w_Dates.push(jsdate.toLocaleTimeString('en', {month: 'short', day: 'numeric', hour: 'numeric', minute:'numeric'}))
      });
      this.chartObj = {
        temp: _t.slice(0,5),
        weatherDates: w_Dates.slice(0,5)
      };
    });
  }

  ngOnDestroy(){
    //unsubscribe the observable.
    this.sub.unsubscribe();
  }

}
