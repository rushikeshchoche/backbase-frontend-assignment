import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-forecast-details',
  templateUrl: './forecast-details.component.html',
  styleUrls: ['./forecast-details.component.scss']
})
export class ForecastDetailsComponent implements OnInit {

  @Input('details') details;
  @Input('day') day;
  constructor() { }

  ngOnInit() {
  }

}
