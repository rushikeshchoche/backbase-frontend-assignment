import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StateService } from '../../services/state-service.service';
import { FeatureService } from 'src/app/services/feature-service.service';
import { appConstant } from '../../app-constants';

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.scss']
})
export class ForecastComponent implements OnInit {

  public forecastData: any;
  public displayForecastData: any;
  public day = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
  public cityId: number;

  constructor (
      private route: ActivatedRoute,
      private router: Router,
      private stateService: StateService,
      private featureService: FeatureService
    ) { }

    ngOnInit() {
      this.route.paramMap.subscribe(params=>{
        this.cityId = parseInt(params.get('cityId'));
        this.getHomeCityForecast(this.cityId);
      });
    }

  /**
   * @name goBack
   * @methodOf Forecast.component
   * @description
   * This method navigates one view back
   */
  goBack() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  /**
   * @name setHomeCity
   * @methodOf Forecast.component
   * @description
   * This method sets home city with cityId
   */
  setHomeCity() {
    this.stateService.setHomeCity(this.cityId);
    this.goBack();
  }

  /**
   * @name getHomeCityForecast
   * @methodOf Forecast.component
   * @param cityId: number
   * @description
   * This method sets home city with cityId
   */
  getHomeCityForecast(cityId: number) {
    const reqParams = {
      params: {
        id: cityId,
        appid: appConstant.APP_ID,
        units: appConstant.UNIT
      }
    }
    this.featureService.httpOperation('getForecastWeather',reqParams).subscribe(data=>{
      this.forecastData = data;
      this.displayForecastData = this.forecastData.list.reduce((result,obj) => {
        let jsdate = new Date(obj.dt * 1000)
        const d = this.day[jsdate.getDay()];
        obj.dt_txt = jsdate;
        if(!result[d]) {
          result[d] = [obj];
        } else {
          result[d].push(obj);
        }
        return result;
      },{});
    });
  }
}
