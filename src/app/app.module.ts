import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OverviewComponent } from './components/overview/overview.component';
import { ForecastComponent } from './components/forecast/forecast.component';
import { ChartComponent } from './components/chart/chart.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { ForecastDetailsComponent } from './components/forecast-details/forecast-details.component';

@NgModule({
  declarations: [
    AppComponent,
    OverviewComponent,
    ForecastComponent,
    ChartComponent,
    HeaderComponent,
    FooterComponent,
    ForecastDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
