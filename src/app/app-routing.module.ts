import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ForecastComponent } from './components/forecast/forecast.component';
import { OverviewComponent } from './components/overview/overview.component';

const routes: Routes = [
  {path: 'home', component: OverviewComponent},
  {path: 'forecast', component: ForecastComponent},
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: '**', component: OverviewComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
