import { Injectable } from '@angular/core';
import {Observable, BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StateService {

  private _homeCity = new BehaviorSubject<String>('');

  homeCity: Observable<String> = this._homeCity.asObservable();

  setHomeCity(val){
    this._homeCity.next(val);
};
}
