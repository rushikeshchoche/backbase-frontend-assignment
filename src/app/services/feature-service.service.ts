import { Injectable } from '@angular/core';
import {ActionService} from './action-service.service'
@Injectable({
  providedIn: 'root'
})
export class FeatureService {

  constructor(private actionService: ActionService) { }

  httpOperation(operation, reqData){
    return this.actionService.httpOperation(operation, reqData);
  }
}
