import { Injectable } from '@angular/core';
import {HttpService} from './http-service.service';
import operationsData from '../../assets/json/operationsData.json';

@Injectable({
  providedIn: 'root'
})
export class ActionService{

  constructor(private httpService: HttpService) { }

  httpOperation(operation,reqData){
    let options = operationsData[operation];
    let httpOptions = {
      method: options.method,
      url: options.url,
      requestDtls: reqData
    }
    return this.httpService.dataOperation(httpOptions);

  }
}
