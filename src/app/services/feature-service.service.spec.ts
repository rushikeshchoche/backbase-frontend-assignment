import { TestBed } from '@angular/core/testing';
import { FeatureService } from './feature-service.service';
import { HttpClientModule } from '@angular/common/http';

describe('FeatureServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule]
  }));

  it('should be created', () => {
    const service: FeatureService = TestBed.get(FeatureService);
    expect(service).toBeTruthy();
  });
});
