# BackbaseFrontendAssignment

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.0.

This application displays current weather displaying the city name plus average temperature and the wind strength of 5 European cities - Amsterdam, Rotterdam, Paris, Vienna and Berlin

Also, you can set Home city of your choice out of above mentioned 5 cities which will be displayed on home screen. Line Graph is plotted for the home city weather for next few hours using chart.js

Clicking on particular city its forecast weather is displayed for next 5 days/3 hour

## Project Structure

Compoents 
  AppComponent - This is the root component of the application
  HeaderComponent - Contains header tag
  FooterComponent - Contains Footer tag
  OverviewComponent - This component displays the list of 5 cities current weather
  ForecastComponent - Displays the Forecast Weather of single city
  ForecastDetailsComponent - Creates tabular view for Forecast data
  ChartComponent - Plots the graph using chart.js

Services
  FeatureService - This service is used by components to call httpOperation method
  ActionService - This service sets the method, url and requestDtls
  HttpService - HttpClient is used to fetch data from API
  StateService - It has an Observable homeCity and a method setHomeCity.


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

